#------------------------ Evaluation using APPA-REAL Dataset ------------------

# python resnet_eval.py --weight_file <Path to weight file>



import cv2
import numpy as np
import pandas as pd
import argparse
from tqdm import tqdm
from pathlib import Path
from model import get_model


def get_args():
    parser = argparse.ArgumentParser(description="APPA-REAL evaluation",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--weight_file", type=str, default=None,
                        help="path to weight file")
    args = parser.parse_args()
    return args


def error(predict, mean, standv):
    error = 1 - np.exp(-((predict - mean) * (predict - mean) / 2 * standv * standv))
    return error


def main():
    args = get_args()
    weight_file = args.weight_file

    # load model and weights
    img_size = 224
    batch_size = 32
    model = get_model(model_name="ResNet50")

    model.load_weights(weight_file)
    dataset_root = Path(__file__).parent.joinpath("appa-real", "appa-real-release")
    validation_image_dir = dataset_root.joinpath("test")
    gt_valid_path = dataset_root.joinpath("gt_avg_test.csv")
    image_paths = list(validation_image_dir.glob("*_face.jpg"))

    faces = np.empty((batch_size, img_size, img_size, 3))
    ages = []
    image_names = []

    for i, image_path in tqdm(enumerate(image_paths)):
        faces[i % batch_size] = cv2.resize(cv2.imread(str(image_path), 1), (img_size, img_size))
        image_names.append(image_path.name[:-9])

        #if (i + 1) % batch_size == 0 or i == len(image_paths) - 1:
        results = model.predict(faces)
        ages_out = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages_out).flatten()
        ages += list(predicted_ages)

    print(len(ages))
    print(len(image_names))
    name2age = {image_names[i]: ages[i] for i in range(len(image_names))}
    df = pd.read_csv(str(gt_valid_path))
    appa_abs_error = 0.0
    real_abs_error = 0.0
    epsilon_error = 0.0


    for i, row in df.iterrows():
        difference1 = name2age[row.file_name] - row.apparent_age_avg
        difference2 = name2age[row.file_name] - row.real_age
        appa_abs_error += abs(difference1)
        real_abs_error += abs(difference2)
        epsilon_error += error(name2age[row.file_name], row.apparent_age_avg, 0.3)

    print("MAE Apparent: {}".format(appa_abs_error / len(image_names)))
    print("MAE Real: {}".format(real_abs_error / len(image_names)))
    print("epsilon-error: {}".format(epsilon_error / len(image_names)))



if __name__ == '__main__':
    main()