# Age__Estimation

The project entitled « Eagle Eye » consists in developing a deep learning model for estimating person's age in a streaming video captured by cameras installed at indoor advertising totems.

1/ Download UTKFace dataset using this link:
 https://susanqq.github.io/UTKFace/

 2/ Download Appa-Real dataset using this link:
 http://chalearnlap.cvc.uab.es/dataset/26/description/

 3/ Using "create_db_utkface.py" to create database for training from the UTKFace dataset

 python create_db_utkface.py  -i <path to the UTKFace image directory> -o <path to output directory>

4/ Train the model using UTKFace and APPA-Real dataset using "train.py"

python train.py --appa_dir <path to APPA-Real Dataset> --utk_dir <path to the UTKFace image directory> --model_name ResNet50 --output_dir <path to output directory> --lr 0.001 --opt sgd --nb_epochs 100

5/ Open the Webcam and check the model after training using "demo_photo_video.py"

python demo_photo_video.py --weight_file <path to weight file>

6/ Plot age_mae history curve using "plot.py"

python plot.py --input1 history

(history is a directory that contain history.npz)

7/ Evaluate the model using APPA-Real Dataset

python resnet_eval.py --weight_file <Path to weight file>


# Elastic Stack

#** Download (version 7.8)**

1/ Download Filebeat
https://www.elastic.co/fr/downloads/beats/filebeat

2/ Download Logstash
https://www.elastic.co/fr/downloads/logstash

3/ Download Elasticsearch
https://www.elastic.co/fr/downloads/elasticsearch

4/ Download Kibana
https://www.elastic.co/fr/downloads/kibana

# **Configuration**

# Start Elastic Stack (OS:Ubuntu)
1) Start Filebeat:

sudo rm -rf data/registry

sudo chown root filebeat.yml

sudo ./filebeat -e

2) Start Logstash

./bin/logstash -f logstash.conf

3) Start Elasticsearch

./bin/elasticsearch

4) Start Kibana

./bin/kibana
# Metric used to evaluate (MAE:Mean Absolute Error)
MAE(Mean Absolute Error) est la moyenne de la différence absolue entre la valeur prévue et la valeur réelle sortie qui est représentée 
comme montre la formule dans l'image"MAE_metric.png" où,Yi est le label(l’âge réel) de l’ ième image et Ŷi représente l’âge estimé en fonction du cadre proposé.
 N est le nombre totald’échantillons d’analyse.

# Age Estimation Using CNN Approach Paper

1/ Age estimation using VGG16 architecture(best MAE:3.252)
https://data.vision.ee.ethz.ch/cvl/publications/papers/articles/eth_biwi_01299.pdf

2/ Age estimation using ResNet50 architecture(best MAE: 8.74)
https://ieeexplore.ieee.org/document/8901521


NB: 
1) "weights.084-3.718-4.119.hdf5" is the best weight file to use in the demo_photo_video.py
Obtained ofter train over 100 epocs  using SGD optimizer and 0.001 learning rate

2) "weights.005-3.295-4.509.hdf5" is the weight file obtained after train
over only 5 epocs using SGD optimizer and 0.001 learning rate

3) "logstash.conf" is the configuration file for logstash
